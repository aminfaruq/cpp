package id.co.maminfaruq.ccp.model.login

import com.google.gson.annotations.SerializedName

data class LoginResponse(

        @SerializedName("code")
        val code : String? = null,

        @SerializedName("message")
        val message : String? = null,

        @SerializedName("data")
        val data : LoginData? = null

)
