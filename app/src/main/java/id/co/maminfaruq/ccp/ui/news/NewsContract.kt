package id.co.maminfaruq.ccp.ui.news

import android.content.Context
import id.co.maminfaruq.ccp.model.login.LoginData
import id.co.maminfaruq.ccp.model.news.ItemNews

interface NewsContract {
    interface View {
        fun showMessage(message: String)
        fun showData(data : List<ItemNews>)
    }

    interface Presenter{
        fun getNews(auth : String)
    }
}