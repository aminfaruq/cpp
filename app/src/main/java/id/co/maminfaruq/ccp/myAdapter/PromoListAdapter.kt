package id.co.maminfaruq.ccp.myAdapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.co.maminfaruq.ccp.R
import id.co.maminfaruq.ccp.model.storeVisit.DataItem
import id.co.maminfaruq.ccp.model.storeVisit.TaskProductItem
import kotlinx.android.synthetic.main.item_promo_list.view.*

class PromoListAdapter(val context: Context, val listPromo: List<DataItem>): RecyclerView.Adapter<PromoListAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        return PromoListAdapter.MyViewHolder(LayoutInflater.from(context).inflate(R.layout.item_promo_list,p0,false))
    }

    override fun getItemCount(): Int {
        return listPromo.size
    }

    override fun onBindViewHolder(p0: MyViewHolder, p1: Int) {
        p0.bindItems(listPromo[p1].taskProduct!![p1])
    }

    class MyViewHolder(view: View): RecyclerView.ViewHolder(view) {

        val tv_title = view.tv_title_promo_list

        fun bindItems(dataItem: TaskProductItem){
            tv_title.text = dataItem.name
        }
    }
}