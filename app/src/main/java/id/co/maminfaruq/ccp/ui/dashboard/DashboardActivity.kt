package id.co.maminfaruq.ccp.ui.dashboard

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import id.co.maminfaruq.ccp.R
import id.co.maminfaruq.ccp.ui.account.AccountFragment
import id.co.maminfaruq.ccp.ui.achievement.AchievementFragment
import id.co.maminfaruq.ccp.ui.home.HomeFragment
import id.co.maminfaruq.ccp.ui.news.NewsFragment
import kotlinx.android.synthetic.main.activity_dashboard.*
import android.content.Intent
import android.support.v7.app.ActionBar
import id.co.maminfaruq.ccp.ui.notification.NotificationActivity


class DashboardActivity : AppCompatActivity() {

    var mHome = HomeFragment()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        val actionBar : ActionBar = supportActionBar!!

       // actionBar.navigationMode = (ActionBar.NAVIGATION_MODE_TABS)

        actionBar.setDisplayShowHomeEnabled(true)
        actionBar.setLogo(R.drawable.cpp_logo_white)
        actionBar.setDisplayUseLogoEnabled(true)

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.frame_container, mHome)
                .commit()

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        return when (item?.itemId) {
            R.id.menu_notificaton -> {
                startActivity(Intent(this, NotificationActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->

        when (item.itemId) {
            R.id.navigation_home -> {
                val fragment = HomeFragment()
                supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frame_container, fragment, fragment.javaClass.simpleName)
                        .commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_dashboard -> {
                val fragment = NewsFragment()
                supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frame_container, fragment, fragment.javaClass.simpleName)
                        .commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_achievement -> {
                val fragment = AchievementFragment()
                supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frame_container,fragment,fragment.javaClass.simpleName)
                        .commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_notifications -> {
                val fragment = AccountFragment()
                supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frame_container,fragment,fragment.javaClass.simpleName)
                        .commit()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

}
