package id.co.maminfaruq.ccp.ui.home


import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.co.maminfaruq.ccp.R
import id.co.maminfaruq.ccp.model.news.ItemNews
import id.co.maminfaruq.ccp.myAdapter.BannerAdapter
import id.co.maminfaruq.ccp.ui.promo.PromoActivity
import id.co.maminfaruq.ccp.ui.storeVisit.storeList.StoreVisitActivity
import id.co.maminfaruq.ccp.ui.visitPlan.VisitPlanActivity
import id.co.maminfaruq.ccp.ui.takeOrder.TakeOrderActivity
import id.co.maminfaruq.ccp.utils.Constants
import kotlinx.android.synthetic.main.fragment_home.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class HomeFragment : Fragment(), HomeContract.View {

    private var homePresenter = HomePresenter(this)
    private var pref: SharedPreferences? = null

    var sampleImages = intArrayOf(R.drawable.gambar1, R.drawable.gambar2)


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        pref = context!!.getSharedPreferences(Constants.pref_name, 0)


        val value = pref?.getString(Constants.KEY_TOKEN,null)
        val finalToken = "Bearer " + value!!
        homePresenter.getHomeBanner(finalToken)


        val name = pref?.getString(Constants.KEY_USERNAME, null)
        val nik = pref?.getString(Constants.KEY_TYPE, null)
        val branch = pref?.getString(Constants.KEY_EMAIL, null)

        tv_name.text = name
        tv_branch.text = branch
        tv_nik.text = nik

        buttonListener()

    }

    private fun buttonListener() {
        btn_store_visit.setOnClickListener {
            startActivity(Intent(context, StoreVisitActivity::class.java))
        }

        btn_visit_plan.setOnClickListener {
            startActivity(Intent(context, VisitPlanActivity::class.java))

        }

        btn_take_order.setOnClickListener {
            startActivity(Intent(context, TakeOrderActivity::class.java))
        }

        btnSemuaPromo.setOnClickListener{
            startActivity(Intent(context, PromoActivity::class.java))
        }

    }


    override fun showProgress() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hideProgress() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun loadData(data: List<ItemNews>) {
        val adapter = BannerAdapter(context!!,data)
        viewPagerBanner.adapter = adapter
    }


}
