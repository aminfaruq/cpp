package id.co.maminfaruq.ccp.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import id.co.maminfaruq.ccp.model.login.LoginData
import id.co.maminfaruq.ccp.ui.login.LoginActivity


class SessionManager {

    private var pref : SharedPreferences? = null
    private var editor : SharedPreferences.Editor? = null
    private lateinit var context: Context

     @SuppressLint("CommitPrefEdits")
     constructor(context: Context) {
         this.context = context

         pref = context.getSharedPreferences(Constants.pref_name,0)

         editor = pref?.edit()
     }


    fun createSession(loginData: LoginData){
        editor?.putBoolean(Constants.KEY_IS_LOGIN,true)
        editor?.putInt(Constants.KEY_ID,loginData.id!!)
        editor?.putString(Constants.KEY_NAME,loginData.name)
        editor?.putString(Constants.KEY_USERNAME,loginData.username)
        editor?.putString(Constants.KEY_EMAIL,loginData.email)
        editor?.putString(Constants.KEY_TYPE,loginData.type)
        editor?.putString(Constants.KEY_TOKEN,loginData.token)

        editor?.commit()
    }

    fun isLogin(): Boolean {
        //mengembalikan nilai bolean dan mengambil data dari shared pref
        return pref!!.getBoolean(Constants.KEY_IS_LOGIN, false)
    }

    fun logout() {
        //mengambil method clear untuk menghapus data shred pref
        editor?.clear()
        //mengeksekusi perintah clear
        editor?.commit()

        //membuat intent untuk berpindah halaman
        val intent = Intent(context, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(intent)
    }

}