package id.co.maminfaruq.ccp.model.news

import com.google.gson.annotations.SerializedName

data class NewsResponse(

        @field:SerializedName("code")
        val code: Int? = null,

        @field:SerializedName("data")
        val data: List<ItemNews>? = null,

        @field:SerializedName("message")
        val message: String? = null
)