package id.co.maminfaruq.ccp.ui.storeVisit.viewPagerFragment.promoList

import id.co.maminfaruq.ccp.model.storeVisit.ResponseDetailProduct
import id.co.maminfaruq.ccp.network.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PromoListPresenter(val model: PromoListContract.View): PromoListContract.Presenter {

    var view: PromoListContract.View? = null

    init {
        view = model
    }

    override fun getData(value: String, id: String) {

        view?.showLoading()
        val apiInterface = ApiClient.create()
        apiInterface.getPromoList(value,id)
                .enqueue(object : Callback<ResponseDetailProduct>{
                    override fun onResponse(call: Call<ResponseDetailProduct>?, response: Response<ResponseDetailProduct>?) {
                        view?.hideLoading()

                        if (response?.body() != null){
                            val success = response.body()?.data
                            view?.showData(success)
                        } else {
                            val erorr = response?.message()

                            view?.showFailureMessage(erorr)
                        }
                    }

                    override fun onFailure(call: Call<ResponseDetailProduct>?, t: Throwable?) {
                        view?.hideLoading()
                        view?.showFailureMessage(t?.message)
                    }
                })

    }
}