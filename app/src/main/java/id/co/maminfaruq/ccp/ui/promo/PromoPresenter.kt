package id.co.maminfaruq.ccp.ui.promo

import id.co.maminfaruq.ccp.model.news.NewsResponse
import id.co.maminfaruq.ccp.network.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PromoPresenter(model : PromoContract.View) : PromoContract.Presenter {

    var view : PromoContract.View? = null

    init {
        view = model
    }

    override fun getPromo(token: String) {

        val apiInterface = ApiClient.create()

        apiInterface.promoList(token)
                .enqueue(object : Callback<NewsResponse>{
                    override fun onResponse(call: Call<NewsResponse>?, response: Response<NewsResponse>?) {
                        if (response?.body() != null){
                            view?.showData(response.body()?.data!!)
                        }
                    }

                    override fun onFailure(call: Call<NewsResponse>?, t: Throwable?) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                })
    }
}