package id.co.maminfaruq.ccp.model.storeVisit

import com.google.gson.annotations.SerializedName


data class KunweListItem(

        @field:SerializedName("kunag")
        val kunag: String? = null,

        @field:SerializedName("addr2")
        val addr2: String? = null,

        @field:SerializedName("addr1")
        val addr1: String? = null,

        @field:SerializedName("kunwe")
        val kunwe: String? = null,

        @field:SerializedName("id")
        val id: Int? = null,

        @field:SerializedName("lastupdate")
        val lastupdate: String? = null,

        @field:SerializedName("name2")
        val name2: String? = null,

        @field:SerializedName("name1")
        val name1: String? = null,

        @field:SerializedName("vkorg")
        val vkorg: String? = null,

        @field:SerializedName("task_template_id")
        val taskTemplateId: Any? = null
)