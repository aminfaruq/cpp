package id.co.maminfaruq.ccp.ui.home

import id.co.maminfaruq.ccp.model.news.NewsResponse
import id.co.maminfaruq.ccp.network.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class HomePresenter(model: HomeContract.View) : HomeContract.Presenter {

    var view: HomeContract.View? = null


    init {
        view = model
    }


    override fun getHomeBanner(value: String) {

        val apiInterface = ApiClient.create()

        apiInterface.homeBanner(value)
                .enqueue(object : Callback<NewsResponse> {

                    override fun onResponse(call: Call<NewsResponse>?, response: Response<NewsResponse>?) {

                        if (response?.body() != null) {
                            val home: NewsResponse = response.body()!!

                            view?.loadData(home.data!!)

                        }

                    }

                    override fun onFailure(call: Call<NewsResponse>?, t: Throwable?) {

                    }

                })
    }
}