package id.co.maminfaruq.ccp.ui.storeVisit.storeList

import id.co.maminfaruq.ccp.model.storeVisit.ResponseDetailProduct
import id.co.maminfaruq.ccp.network.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StoreVisitPresenter(model: StoreVisitContract.View): StoreVisitContract.Presenter {

    var view: StoreVisitContract.View? = null

    init {
        view = model
    }


    override fun getData(id: String, value: String) {

        view?.showLoading()
        val apiInterface = ApiClient.create()
        apiInterface.getListStore(value,id)
                .enqueue(object : Callback<ResponseDetailProduct>{
                    override fun onResponse(call: Call<ResponseDetailProduct>?, response: Response<ResponseDetailProduct>?) {
                        view?.hideLoading()
                        if (response?.body() != null){
                            val sucess = response.body()?.data
                            view?.showData(sucess)
                        } else {
                            val error = response?.message()
                            view?.showFailureMessage(error)
                        }
                    }

                    override fun onFailure(call: Call<ResponseDetailProduct>?, t: Throwable?) {
                        view?.hideLoading()
                        view?.showFailureMessage(t?.message)

                    }
                })

    }
}