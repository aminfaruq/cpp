package id.co.maminfaruq.ccp.model.login

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class LoginData(
        @SerializedName("id")
        val id: Int? = null,

        @SerializedName("name")
        val name: String? = null,

        @SerializedName("username")
        val username: String? = null,

        @SerializedName("email")
        val email: String? = null,

        @SerializedName("type")
        val type: String? = null,

        @SerializedName("token")
        val token: String? = null


        ) : Parcelable {
        constructor(source: Parcel) : this(
                source.readValue(Int::class.java.classLoader) as Int?,
                source.readString(),
                source.readString(),
                source.readString(),
                source.readString(),
                source.readString()
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
                writeValue(id)
                writeString(name)
                writeString(username)
                writeString(email)
                writeString(type)
                writeString(token)
        }

        companion object {
                @JvmField
                val CREATOR: Parcelable.Creator<LoginData> = object : Parcelable.Creator<LoginData> {
                        override fun createFromParcel(source: Parcel): LoginData = LoginData(source)
                        override fun newArray(size: Int): Array<LoginData?> = arrayOfNulls(size)
                }
        }
}