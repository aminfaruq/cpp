package id.co.maminfaruq.ccp.ui.account


import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.co.maminfaruq.ccp.R
import id.co.maminfaruq.ccp.R.id.*
import id.co.maminfaruq.ccp.model.login.LoginData
import id.co.maminfaruq.ccp.utils.Constants
import id.co.maminfaruq.ccp.utils.SessionManager
import kotlinx.android.synthetic.main.fragment_account.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class AccountFragment : Fragment(), AccountContract.View {

    var pref: SharedPreferences? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_account, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        edt_name.isFocusableInTouchMode = false
        edt_nik.isFocusableInTouchMode = false
        edt_branch.isFocusableInTouchMode = false

        showUser()

        btn_log_out.setOnClickListener {
            val mSessionManager = SessionManager(context!!)
            mSessionManager.logout()
        }

    }


    private fun showUser() {
        pref = context!!.getSharedPreferences(Constants.pref_name, 0)

        val name = pref?.getString(Constants.KEY_USERNAME, null)
        val nik = pref?.getString(Constants.KEY_TYPE, null)
        val branch = pref?.getString(Constants.KEY_EMAIL,null)

        edt_name.setText(name)
        edt_nik.setText(nik)
        edt_branch.setText(branch)

    }

    override fun showDataUser(loginData: LoginData) {

    }


}
