package id.co.maminfaruq.ccp.ui.promo

import id.co.maminfaruq.ccp.model.news.ItemNews

interface PromoContract {

    interface View{
        fun showData(data : List<ItemNews>)
    }

    interface Presenter{
        fun getPromo(token : String)
    }
}