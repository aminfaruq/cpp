package id.co.maminfaruq.ccp.myAdapter

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.squareup.picasso.Picasso
import id.co.maminfaruq.ccp.model.news.ItemNews

class BannerAdapter(val context: Context, val homeList: List<ItemNews>?) : PagerAdapter() {
    override fun isViewFromObject(view: View, any: Any): Boolean {
        return view == any
    }

    override fun getCount(): Int {
        return homeList?.size!!
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val imageView = ImageView(context)

        Picasso
                .get()
                .load(homeList?.get(position)?.image)
                .fit()
                .centerCrop()
                .into(imageView)

        container.addView(imageView)
        return imageView
    }

    override fun destroyItem(container: ViewGroup, position: Int, any: Any) {
        container.removeView(any as View)
    }
}