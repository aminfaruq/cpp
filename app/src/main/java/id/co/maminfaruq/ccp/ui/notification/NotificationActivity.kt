package id.co.maminfaruq.ccp.ui.notification

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import id.co.maminfaruq.ccp.R

class NotificationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)
        title = "Notification"
    }
}
