package id.co.maminfaruq.ccp.network

import id.co.maminfaruq.ccp.utils.Constants
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient {

    fun create(): ApiInterface {

        //membuat object logging interceptor
        val logging = HttpLoggingInterceptor()
        // set log level
        logging.level = HttpLoggingInterceptor.Level.BODY
        // membuat object httpClient
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(logging)

        val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Constants.baseURL!!)
                .client(httpClient.build())
                .build()

        return retrofit.create(ApiInterface::class.java)

    }
}

