package id.co.maminfaruq.ccp.model.storeVisit

import com.google.gson.annotations.SerializedName


data class DataItem(

        @field:SerializedName("visit_plan_id")
        val visitPlanId: Int? = null,

        @field:SerializedName("notes")
        val notes: String? = null,

        @field:SerializedName("address")
        val address: String? = null,

        @field:SerializedName("confirmflag")
        val confirmflag: String? = null,

        @field:SerializedName("latitude")
        val latitude: String? = null,

        @field:SerializedName("kunwe")
        val kunwe: String? = null,

        @field:SerializedName("task_product")
        val taskProduct: List<TaskProductItem>? = null,

        @field:SerializedName("title")
        val title: String? = null,

        @field:SerializedName("kunwe_list")
        val kunweList: List<KunweListItem?>? = null,

        @field:SerializedName("kunag")
        val kunag: String? = null,

        @field:SerializedName("user_id")
        val userId: Int? = null,

        @field:SerializedName("phone")
        val phone: String? = null,

        @field:SerializedName("contact")
        val contact: String? = null,

        @field:SerializedName("customer_name")
        val customerName: String? = null,

        @field:SerializedName("task_list")
        val taskList: List<TaskListItem?>? = null,

        @field:SerializedName("vkorg")
        val vkorg: String? = null,

        @field:SerializedName("visit_date")
        val visitDate: String? = null,

        @field:SerializedName("longitude")
        val longitude: String? = null
)