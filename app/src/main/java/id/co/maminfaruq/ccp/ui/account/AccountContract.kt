package id.co.maminfaruq.ccp.ui.account

import android.content.Context
import id.co.maminfaruq.ccp.model.login.LoginData

interface AccountContract {

    interface View{
        fun showDataUser(loginData: LoginData)
    }

    interface Presenter{
        fun getDataUser(context: Context)
    }
}