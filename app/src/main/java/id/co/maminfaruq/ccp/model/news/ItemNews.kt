package id.co.maminfaruq.ccp.model.news

import com.google.gson.annotations.SerializedName

data class ItemNews(

	@field:SerializedName("date")
	val date: String? = null,

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("thumbnail")
	val thumbnail: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("content")
	val content: String? = null,

	@field:SerializedName("status")
	val status: Int? = null,

	@field:SerializedName("link")
	val link : String? = null,

	@field:SerializedName("vkorg")
	val vkorg : String? = null,

	@field:SerializedName("werks")
	val werks : String? = null,

	@field:SerializedName("matnr")
	val matnr : String? = null,

	@field:SerializedName("start_date")
	val start_date : String? = null,

	@field:SerializedName("end_date")
	val end_date : String? = null,

	@field:SerializedName("product_name")
	val product_name : String? = null
)