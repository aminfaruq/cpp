package id.co.maminfaruq.ccp.ui.news

import android.content.Context
import android.content.SharedPreferences
import id.co.maminfaruq.ccp.R.layout.news
import id.co.maminfaruq.ccp.network.ApiClient
import id.co.maminfaruq.ccp.utils.Constants

import id.co.maminfaruq.ccp.model.login.LoginData
import id.co.maminfaruq.ccp.model.news.NewsResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewsPresenter(model: NewsContract.View) : NewsContract.Presenter {

    var view: NewsContract.View? = null
    //var loginData : LoginData? = null


    init {
        view = model
    }

    override fun getNews(auth: String) {
        val apiInterface = ApiClient.create()

        apiInterface.getNews(auth)
                .enqueue(object : Callback<NewsResponse> {
                    override fun onResponse(call: Call<NewsResponse>?, response: Response<NewsResponse>?) {

                        val news: NewsResponse = response?.body()!!

                        if (response.isSuccessful) {
                            view?.showData(news.data!!)
                        } else {
                            view?.showMessage(news.message!!)
                        }

                    }

                    override fun onFailure(call: Call<NewsResponse>?, t: Throwable?) {
                        view?.showMessage(t?.message!!)
                    }


                })

    }
}



