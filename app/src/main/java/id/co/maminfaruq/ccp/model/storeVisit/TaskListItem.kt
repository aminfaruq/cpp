package id.co.maminfaruq.ccp.model.storeVisit

import com.google.gson.annotations.SerializedName


data class TaskListItem(

        @field:SerializedName("name")
        val name: String? = null,

        @field:SerializedName("id")
        val id: Int? = null,

        @field:SerializedName("status")
        val status: Int? = null
)