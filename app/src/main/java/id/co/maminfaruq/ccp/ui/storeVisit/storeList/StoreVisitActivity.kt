package id.co.maminfaruq.ccp.ui.storeVisit.storeList

import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import id.co.maminfaruq.ccp.R
import id.co.maminfaruq.ccp.model.storeVisit.DataItem
import id.co.maminfaruq.ccp.myAdapter.StoreListAdapter
import id.co.maminfaruq.ccp.ui.storeVisit.StoreOrderActivity
import id.co.maminfaruq.ccp.utils.Constants
import kotlinx.android.synthetic.main.activity_store_visit.*
import org.jetbrains.anko.startActivity

class StoreVisitActivity : AppCompatActivity(), StoreVisitContract.View {

    private var storePresenter = StoreVisitPresenter(this)
    var pref: SharedPreferences? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store_visit)
        title = "Store Visit"

        getData()

    }

    private fun getData() {
        pref = this.getSharedPreferences(Constants.pref_name,0)

        val value = pref?.getString(Constants.KEY_TOKEN,null)
        val id = pref?.getInt(Constants.KEY_ID,0)
        val finaToken = "Bearer " + value!!

        storePresenter.getData(finaToken, id.toString())

    }

    override fun showLoading() {

    }

    override fun hideLoading() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showData(list: List<DataItem>?) {
        rv_storeList.hasFixedSize()
        rv_storeList.layoutManager = LinearLayoutManager(this)
        rv_storeList.adapter = StoreListAdapter(this, list!!){
            startActivity(Intent(this,StoreOrderActivity::class.java))
        }

    }

    override fun showFailureMessage(msg: String?) {
        Toast.makeText(this,"Failed",Toast.LENGTH_SHORT).show()

    }
}
