package id.co.maminfaruq.ccp.myAdapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import id.co.maminfaruq.ccp.R
import id.co.maminfaruq.ccp.model.news.ItemNews
import kotlinx.android.synthetic.main.news.view.*

class NewsAdapter(val context: Context, val newsList: List<ItemNews>) : RecyclerView.Adapter<NewsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(viewGroub: ViewGroup, position: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.news, viewGroub, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return newsList.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bindItems(newsList[position])
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_title = view.tv_title
        val tv_content = view.tv_content
        val tv_date = view.tv_date
        val img_news = view.img_news

        fun bindItems(newsData: ItemNews) {
            tv_title.text = newsData.title
            tv_content.text = newsData.content
            tv_date.text = newsData.date
            Glide.with(itemView.context)
                    .load("http://mobapp-dev.cpp.co.id:8443/uploads/" + newsData.image)
                    .into(img_news)
        }

    }
}