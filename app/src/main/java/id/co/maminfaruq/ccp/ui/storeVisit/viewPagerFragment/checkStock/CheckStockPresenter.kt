package id.co.maminfaruq.ccp.ui.storeVisit.viewPagerFragment.checkStock

import android.view.View
import id.co.maminfaruq.ccp.model.storeVisit.ResponseDetailProduct
import id.co.maminfaruq.ccp.network.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CheckStockPresenter(var model: CheckStockContract.View): CheckStockContract.Presenter {

    var view :  CheckStockContract.View? = null

    init {
        view = model
    }


    override fun getData(value: String, id: String) {

        view?.showLoading()
        val apiInterface = ApiClient.create()
        apiInterface.getCheckStock(value,id)
                .enqueue(object : Callback<ResponseDetailProduct>{

                    override fun onResponse(call: Call<ResponseDetailProduct>?, response: Response<ResponseDetailProduct>?) {
                        view?.hideLoading()
                        if (response?.body() != null){
                            var sucess = response?.body()?.data
                            view?.showData(sucess)
                        } else{
                            var error = response?.message()
                            view?.showFaliureMessage(error)
                        }
                    }

                    override fun onFailure(call: Call<ResponseDetailProduct>?, t: Throwable?) {
                        view?.hideLoading()
                        view?.showFaliureMessage(t.toString())
                    }

                })
    }
}