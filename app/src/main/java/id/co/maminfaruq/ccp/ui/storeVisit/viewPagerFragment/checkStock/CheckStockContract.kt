package id.co.maminfaruq.ccp.ui.storeVisit.viewPagerFragment.checkStock

import id.co.maminfaruq.ccp.model.storeVisit.DataItem

interface CheckStockContract {

    interface View{
        fun showLoading()
        fun hideLoading()
        fun showData(list: List<DataItem>?)
        fun showFaliureMessage(msg: String?)
    }

    interface Presenter{
        fun getData(value: String, id: String)
    }
}