package id.co.maminfaruq.ccp.ui.promo

import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import id.co.maminfaruq.ccp.R
import id.co.maminfaruq.ccp.model.news.ItemNews
import id.co.maminfaruq.ccp.myAdapter.PromoAdapter
import id.co.maminfaruq.ccp.ui.home.HomePresenter
import id.co.maminfaruq.ccp.utils.Constants
import kotlinx.android.synthetic.main.activity_promo.*

class PromoActivity : AppCompatActivity(), PromoContract.View {

    private var presenter = PromoPresenter(this)


    private var pref: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_promo)
        title = "Promo"

        pref = this.getSharedPreferences(Constants.pref_name, 0)
        val value = pref?.getString(Constants.KEY_TOKEN, null)
        val finalToken = "Bearer " + value!!

        presenter.getPromo(finalToken)
    }

    override fun showData(data: List<ItemNews>) {
        rv_promo.layoutManager = GridLayoutManager(this, 2)
        rv_promo.adapter = PromoAdapter(this, data)
    }
}
