package id.co.maminfaruq.ccp.ui.login

import android.content.Context
import id.co.maminfaruq.ccp.model.login.LoginData

interface LoginContract {
    interface View{
        fun loginSuccess( message : String, loginData: LoginData)
        fun loginFailure( message: String)
        fun email(message: String)
        fun password(message: String)
        fun isLogin()

    }

    interface Presenter{
        fun doLogin( email : String, password : String)
        fun saveDataUser(context: Context, loginData: LoginData )
        fun checkLogin(context: Context)
    }
}