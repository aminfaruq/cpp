package id.co.maminfaruq.ccp.myAdapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import id.co.maminfaruq.ccp.R

import id.co.maminfaruq.ccp.model.news.ItemNews
import kotlinx.android.synthetic.main.news.view.*

class HomeAdapter(val context : Context, val homeList: List<ItemNews>) : RecyclerView.Adapter<HomeAdapter.ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.news, p0, false)
        return HomeAdapter.ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return homeList.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.bindItems(homeList[p1])
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_title = view.tv_title
        val img_news = view.img_news

        fun bindItems(newsData: ItemNews) {
            tv_title.text = newsData.title
            Glide.with(itemView.context)
                    .load(newsData.image)
                    .into(img_news)
        }

    }

}