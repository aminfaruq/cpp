package id.co.maminfaruq.ccp.ui.storeVisit.viewPagerFragment.productList

import id.co.maminfaruq.ccp.model.storeVisit.DataItem

interface ProductListContract {


interface View{
        fun showLoading()
        fun hideLoading()
        fun showData(list: List<DataItem>?)
        fun showFailureMessage(msg: String?)

    }

    interface Presenter{
        fun getData(value: String, id: String)
    }
}