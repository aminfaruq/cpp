package id.co.maminfaruq.ccp.myAdapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.co.maminfaruq.ccp.R
import id.co.maminfaruq.ccp.model.news.ItemNews
import kotlinx.android.synthetic.main.item_promo.view.*

class PromoAdapter(val context: Context, val newsList: List<ItemNews>) : RecyclerView.Adapter<PromoAdapter.ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_promo, p0, false)
        return PromoAdapter.ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return newsList.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.bindItems(newsList[p1])
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_promo = view.tv_judul_product

        fun bindItems(newsData: ItemNews) {
          tv_promo.text = newsData.product_name
        }
    }
}