package id.co.maminfaruq.ccp.model.storeVisit

import com.google.gson.annotations.SerializedName


data class ResponseDetailProduct(

        @field:SerializedName("code")
        val code: Int? = null,

        @field:SerializedName("data")
        val data: List<DataItem>? = null,

        @field:SerializedName("message")
        val message: String? = null
)