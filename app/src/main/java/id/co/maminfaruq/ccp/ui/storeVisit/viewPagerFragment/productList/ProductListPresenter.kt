package id.co.maminfaruq.ccp.ui.storeVisit.viewPagerFragment.productList

import id.co.maminfaruq.ccp.model.storeVisit.ResponseDetailProduct
import id.co.maminfaruq.ccp.network.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductListPresenter(var model: ProductListContract.View): ProductListContract.Presenter {

    var view: ProductListContract.View? = null

    init {
       view = model
    }


    override fun getData(value: String, id: String) {

        view?.showLoading()
        val apiInterface = ApiClient.create()
        apiInterface.getProductList(value,id)
                .enqueue(object : Callback<ResponseDetailProduct>{
                    override fun onResponse(call: Call<ResponseDetailProduct>?, response: Response<ResponseDetailProduct>?) {
                        view?.hideLoading()
                        if (response?.body() != null){

                            val sucess = response.body()?.data
                            view?.showData(sucess)
                        } else {
                            val error = response?.message()
                            view?.showFailureMessage(error)
                        }
                    }

                    override fun onFailure(call: Call<ResponseDetailProduct>?, t: Throwable?) {
                        view?.hideLoading()
                        view?.showFailureMessage(t?.message)
                    }

                })
    }
}