package id.co.maminfaruq.ccp.ui.storeVisit.viewPagerFragment.productList


import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import id.co.maminfaruq.ccp.R
import id.co.maminfaruq.ccp.model.storeVisit.DataItem
import id.co.maminfaruq.ccp.myAdapter.ProductListAdapter
import id.co.maminfaruq.ccp.utils.Constants
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_product_list.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class ProductListFragment : Fragment(), ProductListContract.View {

    private var productListPresenter = ProductListPresenter(this)
    private var pref: SharedPreferences? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_product_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        pref = context?.getSharedPreferences(Constants.pref_name,0)

        val value = pref?.getString(Constants.KEY_TOKEN,null)
        val id = pref?.getInt(Constants.KEY_ID,0)
        val finalToken = "Bearer " + value!!

        productListPresenter.getData(finalToken,id.toString())
    }

    override fun showLoading() {
        Toast.makeText(context,"Sucess Load",Toast.LENGTH_SHORT).show()
    }

    override fun hideLoading() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showData(list: List<DataItem>?) {
        rv_product_list.hasFixedSize()
        rv_product_list.adapter = ProductListAdapter(context!!, list!!)
        rv_product_list.layoutManager = GridLayoutManager(context,2)
    }

    override fun showFailureMessage(msg: String?) {
        Toast.makeText(context,"Failed Load",Toast.LENGTH_SHORT).show()
    }


}
