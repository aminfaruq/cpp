package id.co.maminfaruq.ccp.myAdapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.OrientationEventListener
import android.view.View
import android.view.ViewGroup
import id.co.maminfaruq.ccp.R
import id.co.maminfaruq.ccp.model.storeVisit.DataItem
import kotlinx.android.synthetic.main.item_store_list.view.*
import org.jetbrains.anko.sdk25.coroutines.onClick

class StoreListAdapter(val context: Context, var storeList: List<DataItem>, val listener: (DataItem) -> Unit): RecyclerView.Adapter<StoreListAdapter.ViewHolder>() {


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {

        return StoreListAdapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_store_list,p0,false))
    }

    override fun getItemCount(): Int {
        return storeList.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.bindItems(storeList[p1],listener)
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        val tv_title = view.tv_store_list

        fun bindItems(storeAdapter: DataItem, listener:(DataItem) -> Unit) {
            tv_title.text = storeAdapter.customerName
            itemView.onClick {
                if (storeAdapter != null){
                 listener(storeAdapter)
                }
            }
        }
    }


}