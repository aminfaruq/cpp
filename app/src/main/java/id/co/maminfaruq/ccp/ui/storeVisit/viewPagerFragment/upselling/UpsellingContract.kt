package id.co.maminfaruq.ccp.ui.storeVisit.viewPagerFragment.upselling

import id.co.maminfaruq.ccp.model.storeVisit.DataItem

interface UpsellingContract {

    interface View{
        fun showLoading()
        fun hideLoading()
        fun showData(list: List<DataItem>?)
        fun showfFailureMessage(msg: String?)
    }

    interface Presenter{
        fun getData(value: String, id: String)
    }
}