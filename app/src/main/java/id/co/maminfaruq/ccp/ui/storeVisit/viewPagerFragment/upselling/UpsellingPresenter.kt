package id.co.maminfaruq.ccp.ui.storeVisit.viewPagerFragment.upselling

import id.co.maminfaruq.ccp.model.storeVisit.ResponseDetailProduct
import id.co.maminfaruq.ccp.network.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UpsellingPresenter(var model: UpsellingContract.View): UpsellingContract.Presenter {

    var view : UpsellingContract.View? = null

    init {
        view = model
    }



    override fun getData(value: String, id: String) {

        view?.showLoading()
        val apiInterface = ApiClient.create()
        apiInterface.getUpselling(value,id)
                .enqueue(object : Callback<ResponseDetailProduct>{
                    override fun onResponse(call: Call<ResponseDetailProduct>?, response: Response<ResponseDetailProduct>?) {

                        view?.hideLoading()

                        if (response?.body() != null){
                            val succes = response.body()?.data
                            view?.showData(succes)
                        } else{
                            val error = response?.message()
                            view?.showfFailureMessage(error)
                        }
                    }

                    override fun onFailure(call: Call<ResponseDetailProduct>?, t: Throwable?) {
                        view?.hideLoading()
                        view?.showfFailureMessage(t?.message)
                    }

                })
    }
}