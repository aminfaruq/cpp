package id.co.maminfaruq.ccp.utils

object Constants {
    val baseURL : String? = "http://mobapp-dev.cpp.co.id:8443/api/"
    val KEY_LOGIN : String? = "KEY_LOGIN"

    val pref_name : String? = "PREF_LOGIN"
    val KEY_IS_LOGIN : String? = "KEY_IS_LOGIN"
    val KEY_ID: String? = "KEY_USER_ID"
    val KEY_NAME : String? = "KEY_NAME"
    val KEY_USERNAME : String? = "USERNAME"
    val KEY_EMAIL : String? = "KEY_EMAIL"
    val KEY_TYPE : String? = "KEY_TYPE"
    val KEY_TOKEN : String? = "KEY_TOKEN"


}