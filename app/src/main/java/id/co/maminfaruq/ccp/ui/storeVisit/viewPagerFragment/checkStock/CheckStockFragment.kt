package id.co.maminfaruq.ccp.ui.storeVisit.viewPagerFragment.checkStock


import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import id.co.maminfaruq.ccp.R
import id.co.maminfaruq.ccp.model.storeVisit.DataItem
import id.co.maminfaruq.ccp.myAdapter.CheckStockAdapter
import id.co.maminfaruq.ccp.utils.Constants
import kotlinx.android.synthetic.main.fragment_check_stock.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class CheckStockFragment : Fragment(),CheckStockContract.View {

    private var stockPresenter = CheckStockPresenter(this)
    private var pref: SharedPreferences? = null




    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_check_stock, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        pref = context?.getSharedPreferences(Constants.pref_name,0)

        val value = pref?.getString(Constants.KEY_TOKEN,null)
        val id = pref?.getInt(Constants.KEY_ID,0)
        val finalToken = "Bearer " + value!!

        stockPresenter.getData(finalToken,id.toString())

    }

    override fun showLoading() {
        Toast.makeText(context,"Succes",Toast.LENGTH_SHORT).show()
    }

    override fun hideLoading() {

    }

    override fun showData(list: List<DataItem>?) {
        rv_check_stock.hasFixedSize()
        rv_check_stock.adapter = CheckStockAdapter(context!!, list!!)
        rv_check_stock.layoutManager = LinearLayoutManager(context)
    }

    override fun showFaliureMessage(msg: String?) {
        Toast.makeText(context,"Failed Load Data",Toast.LENGTH_SHORT).show()
    }





}
