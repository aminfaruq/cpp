package id.co.maminfaruq.ccp.ui.home

import id.co.maminfaruq.ccp.model.news.ItemNews

interface HomeContract {

    interface View{
        fun showProgress()
        fun hideProgress()
        fun loadData( data : List<ItemNews>)
    }

    interface Presenter{
        fun getHomeBanner(value : String)
    }
}