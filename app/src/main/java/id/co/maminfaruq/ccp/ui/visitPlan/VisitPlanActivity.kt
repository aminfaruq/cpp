package id.co.maminfaruq.ccp.ui.visitPlan

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import id.co.maminfaruq.ccp.R
import id.co.maminfaruq.ccp.ui.dashboard.DashboardActivity
import kotlinx.android.synthetic.main.activity_visit_plan.*
import org.jetbrains.anko.startActivity

class VisitPlanActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_visit_plan)
        title = "Visit Plan"

        toolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_)
        toolbar.setNavigationOnClickListener {
            startActivity(Intent(this,DashboardActivity::class.java))
        }
        setSupportActionBar(toolbar)


    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.calendar,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        return when (item?.itemId) {
            R.id.calendar -> {

                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    }
}
