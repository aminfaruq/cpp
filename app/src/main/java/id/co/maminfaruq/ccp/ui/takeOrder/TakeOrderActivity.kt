package id.co.maminfaruq.ccp.ui.takeOrder

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import id.co.maminfaruq.ccp.R

class TakeOrderActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_take_order)
        title = "Take Order"
    }
}
