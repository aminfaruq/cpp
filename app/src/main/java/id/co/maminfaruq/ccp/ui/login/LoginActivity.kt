package id.co.maminfaruq.ccp.ui.login

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import id.co.maminfaruq.ccp.utils.Constants
import id.co.maminfaruq.ccp.ui.dashboard.DashboardActivity
import id.co.maminfaruq.ccp.R
import id.co.maminfaruq.ccp.model.login.LoginData
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity(), LoginContract.View {

    private lateinit var loginPresenter: LoginPresenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        loginPresenter = LoginPresenter(this)

        loginPresenter.checkLogin(this)


        login()
    }

    private fun login() {
        btn_login.setOnClickListener {
            loginPresenter.doLogin(edt_email.text.toString(), edt_password.text.toString())
        }
    }


    override fun loginSuccess(message: String, loginData: LoginData) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        loginPresenter = LoginPresenter(this)

        loginPresenter.saveDataUser(this,loginData)

        val mLoginData = LoginData()
        mLoginData.id
        mLoginData.name
        mLoginData.email
        mLoginData.type
        mLoginData.token

        startActivity(Intent(this, DashboardActivity::class.java).putExtra(Constants.KEY_LOGIN, mLoginData))
        finish()

    }

    override fun loginFailure(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun email(message: String) {
        edt_email.error = message
    }

    override fun password(message: String) {
        edt_password.error = message
    }

    override fun isLogin() {
        startActivity(Intent(this, DashboardActivity::class.java))

        finish()
    }
}
