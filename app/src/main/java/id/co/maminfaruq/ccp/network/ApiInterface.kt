package id.co.maminfaruq.ccp.network

import id.co.maminfaruq.ccp.model.login.LoginResponse
import id.co.maminfaruq.ccp.model.news.NewsResponse
import id.co.maminfaruq.ccp.model.storeVisit.ResponseDetailProduct
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {

    @FormUrlEncoded
    @POST("auth/login")
    fun login(
            @Field("email") email: String,
            @Field("password") password: String
    ): Call<LoginResponse>


    @GET("news/list")
    fun getNews(
            @Header("Authorization") auth: String
    ): Call<NewsResponse>

    @GET("banner/list")
    fun homeBanner(
            @Header("Authorization") auth: String
    ): Call<NewsResponse>

    @GET("promo/list")
    fun promoList(
            @Header("Authorization") auth: String
    ): Call<NewsResponse>

    @FormUrlEncoded
    @POST("customer/store-visit-list")
    fun getListStore(
            @Header("Authorization") auth: String,
            @Field("user_id") userId: String
    ): Call<ResponseDetailProduct>

    @FormUrlEncoded
    @POST("customer/store-visit-list")
    fun getCheckStock(
            @Header("Authorization") auth: String,
            @Field("user_id") userId: String
    ): Call<ResponseDetailProduct>

    @FormUrlEncoded
    @POST("customer/store-visit-list")
    fun getProductList(
            @Header("Authorization") auth: String,
            @Field("user_id") userId: String
    ): Call<ResponseDetailProduct>

    @FormUrlEncoded
    @POST("customer/store-visit-list")
    fun getPromoList(
            @Header("Authorization") auth: String,
            @Field("user_id") userId: String
    ): Call<ResponseDetailProduct>

    @FormUrlEncoded
    @POST("customer/store-visit-list")
    fun getUpselling(
            @Header("Authorization") auth: String,
            @Field("user_id") userId: String
    ): Call<ResponseDetailProduct>


}