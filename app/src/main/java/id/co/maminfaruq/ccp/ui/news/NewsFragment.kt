package id.co.maminfaruq.ccp.ui.news


import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import id.co.maminfaruq.ccp.R
import id.co.maminfaruq.ccp.model.news.ItemNews
import id.co.maminfaruq.ccp.myAdapter.NewsAdapter
import id.co.maminfaruq.ccp.utils.Constants
import kotlinx.android.synthetic.main.fragment_news.*


/**
 * A simple [Fragment] subclass.
 *
 */
class NewsFragment : Fragment(), NewsContract.View {

    private var newsPresenter = NewsPresenter(this)
    var pref: SharedPreferences? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_news, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        pref = context!!.getSharedPreferences(Constants.pref_name, 0)

        val value = pref?.getString(Constants.KEY_TOKEN,null)

        val finalToken = "Bearer " + value!!
        //newsPresenter.getNews(finalToken)

    }

    override fun showMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun showData(data: List<ItemNews>) {
        rv_news.hasFixedSize()
        rv_news.layoutManager = LinearLayoutManager(context)
        rv_news.adapter = NewsAdapter(context!!, data)
    }


}
