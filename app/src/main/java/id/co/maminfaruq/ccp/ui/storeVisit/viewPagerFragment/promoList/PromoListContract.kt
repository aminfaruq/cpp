package id.co.maminfaruq.ccp.ui.storeVisit.viewPagerFragment.promoList

import id.co.maminfaruq.ccp.model.storeVisit.DataItem

interface PromoListContract {

    interface View{
        fun showLoading()
        fun hideLoading()
        fun showData(list: List<DataItem>?)
        fun showFailureMessage(msg: String?)
    }

    interface Presenter{
        fun getData(value: String, id: String)
    }
}