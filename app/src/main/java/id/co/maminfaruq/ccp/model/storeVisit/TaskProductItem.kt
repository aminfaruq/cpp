package id.co.maminfaruq.ccp.model.storeVisit


import com.google.gson.annotations.SerializedName


data class TaskProductItem(

        @field:SerializedName("end_date")
        val endDate: String? = null,

        @field:SerializedName("image")
        val image: String? = null,

        @field:SerializedName("add_stock")
        val addStock: String? = null,

        @field:SerializedName("last_stock")
        val lastStock: String? = null,

        @field:SerializedName("matnr")
        val matnr: String? = null,

        @field:SerializedName("description")
        val description: String? = null,

        @field:SerializedName("food_type")
        val foodType: String? = null,

        @field:SerializedName("type")
        val type: String? = null,

        @field:SerializedName("pstyv")
        val pstyv: String? = null,

        @field:SerializedName("werks")
        val werks: String? = null,

        @field:SerializedName("division")
        val division: String? = null,

        @field:SerializedName("vrkme")
        val vrkme: String? = null,

        @field:SerializedName("flag_q")
        val flagQ: String? = null,

        @field:SerializedName("current_stock")
        val currentStock: String? = null,

        @field:SerializedName("qty")
        val qty: Int? = null,

        @field:SerializedName("name")
        val name: String? = null,

        @field:SerializedName("max_shelf_life")
        val maxShelfLife: Int? = null,

        @field:SerializedName("brand")
        val brand: String? = null,

        @field:SerializedName("vkorg")
        val vkorg: String? = null,

        @field:SerializedName("start_date")
        val startDate: String? = null
)