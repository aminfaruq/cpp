package id.co.maminfaruq.ccp.ui.login

import android.content.Context
import android.util.Log
import id.co.maminfaruq.ccp.utils.SessionManager
import id.co.maminfaruq.ccp.model.login.LoginData
import id.co.maminfaruq.ccp.model.login.LoginResponse
import id.co.maminfaruq.ccp.network.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginPresenter(model: LoginContract.View) : LoginContract.Presenter {

    var view: LoginContract.View? = null
    lateinit var mSessionManager: SessionManager

    init {
        view = model
    }

    override fun doLogin(email: String, password: String) {

        if (email.isEmpty() || password.isEmpty()) {
            view?.email("Email is Empty")
            view?.password("Password is Empty")
            return
        }

        /*if (password.isEmpty()) {
            view?.password("Password is Empty")
            return
        }*/

        val apiInterface = ApiClient.create()
        apiInterface.login(email, password)
                .enqueue(object : Callback<LoginResponse> {

                    override fun onResponse(call: Call<LoginResponse>?, response: Response<LoginResponse>?) {
                        if (response?.body() != null) {
                            if (response.body()?.data != null && response.isSuccessful) {
                                val loginData: LoginData = response.body()?.data!!
                                val message: String = response.body()?.message!!
                                view?.loginSuccess(message, loginData)

                            } else {
                                view?.loginFailure("Data is Empty")
                            }
                        }
                    }

                    override fun onFailure(call: Call<LoginResponse>?, t: Throwable?) {
                        view?.loginFailure(t?.message!!)
                        Log.e(" gagal ", t?.message)
                    }

                })
    }

    override fun saveDataUser(context: Context, loginData: LoginData) {
        mSessionManager = SessionManager(context)

        mSessionManager.createSession(loginData)
    }

    override fun checkLogin(context: Context) {
        mSessionManager = SessionManager(context)

        val isLogin: Boolean = mSessionManager.isLogin()

        if (isLogin) {
            view?.isLogin()
        }
    }
}